CC=clang
CXX=clang++

CFLAGS=-W -Wall -Wextra -fPIC -I include/ 
LDFLAGS= 

SRC=src/Pathway.cpp src/Vec3.cpp src/lq.c
OBJ=src/Pathway.o src/Vec3.o src/lq.o
HEADER=OpenSteer/AbstractVehicle.h  OpenSteer/Color.h  OpenSteer/LocalSpace.h  OpenSteer/lq.h  OpenSteer/Obstacle.h  OpenSteer/Pathway.h  OpenSteer/Proximity.h  OpenSteer/SteerLibrary.h  OpenSteer/Utilities.h  OpenSteer/Vec3.h

PREFIX=/usr

.PHONY: all install clean clean-dist example

all: libOpenSteer.so

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

%.o: %.cpp
	$(CXX) -c $(CFLAGS) $< -o $@

libOpenSteer.so: $(OBJ)
	$(CXX) -shared $(LDFLAGS) $^ -o $@

install: libOpenSteer.so
	cp $< $(PREFIX)/lib/$<
	mkdir -p $(PREFIX)/include/OpenSteer
	for f in $(HEADER); do cp include/$$f $(PREFIX)/include/OpenSteer/.; done

example:
	make -C linux

clean:
	@$(RM) $(OBJ)

clean-dist: clean
	@$(RM) libOpenSteer.so
